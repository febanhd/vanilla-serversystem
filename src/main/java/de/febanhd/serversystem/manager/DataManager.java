package de.febanhd.serversystem.manager;

import com.google.common.collect.Lists;
import de.febanhd.serversystem.sql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class DataManager {

    private MySQL mySQL;

    public DataManager(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public void checkPlayer(Player player) {
        String uuid = player.getUniqueId().toString();
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + uuid + "'");
            if (!this.isRegistered(player.getUniqueId())) {
                mySQL.update("INSERT INTO `serversystem` (`UUID`, `team`, `canPlay`, `name`, `request`, `requestTeam`) VALUES" +
                        " ('" + uuid + "','" + Teams.NOPLAYER.toString() + "',false, '" + player.getName() + "', false, '" + Teams.NOPLAYER.toString() + "')");
            }
            if(player.isOp()) {
                mySQL.update("UPDATE `serversystem` SET `team`='" + Teams.ADMIN.toString() + "', `canPlay`=true WHERE UUID = '" + uuid + "'");
            }
            mySQL.update("UPDATE `serversystem` SET `name`='" + player.getName() + "' WHERE UUID = '" + uuid + "'");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isRegistered(UUID uuid) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + uuid.toString() + "'");
            while(resultSet.next()) {
                return resultSet.getString("name") != null;
            }
        }catch (SQLException e) {
            return false;
        }
        return false;
    }

    public boolean canPlay(UUID uuid) {
        boolean canPlay = false;
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + uuid.toString() + "'");
            while(resultSet.next()) {
                canPlay = resultSet.getBoolean("canPlay");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return canPlay;
    }

    public boolean requestForPlayer(Player player, Teams team) {
        String uuid = player.getUniqueId().toString();
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + uuid + "'");
            while(resultSet.next()) {
                if(!resultSet.getBoolean("request")) {
                    mySQL.update("UPDATE `serversystem` SET `request`=true, `requestTeam`='" + team.toString() + "' WHERE UUID = '" + uuid + "'");
                    return true;
                }else {
                    return false;
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean hasRequestSended(String playerName) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + this.getUUID(playerName) + "'");
            while(resultSet.next()) {
                return resultSet.getBoolean("request");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public UUID getUUID(String playerName) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem`");
            while(resultSet.next()) {
                String name = resultSet.getString("name");
                if(name.equalsIgnoreCase(playerName)) {
                    ResultSet r = mySQL.query("SELECT * FROM `serversystem` WHERE name = '" + name + "'");
                    while(r.next()) {
                        return UUID.fromString(r.getString("UUID"));
                    }
                }
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void acceptPlayer(UUID uuid) {
        Teams team = Teams.NOPLAYER;
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + uuid.toString() + "'");
            while (resultSet.next()) {
                team = Teams.valueOf(resultSet.getString("requestTeam"));
            }
            mySQL.update("UPDATE `serversystem` SET `request`=false, `team`='" + team.toString() + "', `canPlay`=true WHERE UUID = '" + uuid.toString() + "'");
        }catch (SQLException e) {
            e.printStackTrace();
        }
        Player player = Bukkit.getPlayer(uuid);
        if(player != null) {
            player.kickPlayer("§7Du bist nun ein Mitspieler in Team " + team.getName());
        }

    }

    public void removePlayer(UUID uuid) {
        mySQL.update("UPDATE `serversystem` SET `canPlay`=false WHERE UUID = '" + uuid.toString() + "'");
    }

    public List<String> getRequestedPlayerNames() {
        List<String> playerNames = Lists.newArrayList();
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE request = true");
            while (resultSet.next()) {
                playerNames.add(resultSet.getString("name"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(playerNames.size() == 0) {
            playerNames.add("§cKeine");
        }
        return playerNames;
    }

    public Teams getTeam(Player player) {
        try {
            ResultSet resultSet = mySQL.query("SELECT * FROM `serversystem` WHERE UUID = '" + player.getUniqueId().toString() + "'");
            while(resultSet.next()) {
                return Teams.valueOf(resultSet.getString("team"));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
