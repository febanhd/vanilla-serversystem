package de.febanhd.serversystem.manager;

import org.bukkit.ChatColor;

public enum Teams {

    ADMIN("00admin", "§4", "§4Admin", ChatColor.DARK_RED),
    ROT("01rot", "§c", "§cRot", ChatColor.RED),
    GELB("02gelb", "§e", "§eGelb", ChatColor.YELLOW),
    GRÜN("03grün", "§2", "§2Grün", ChatColor.GREEN),
    BLAU("04blau", "§9", "§9Blau", ChatColor.BLUE),
    NOPLAYER("05noplayer", "§8K-S: ", "§8Keins", ChatColor.DARK_GRAY);


    private String teamName, prefix, name;
    private ChatColor color;

   Teams(String teamName, String prefix, String name, ChatColor color) {
        this.teamName = teamName;
        this.prefix = prefix;
        this.name = name;
        this.color = color;
    }

    public String getPrefix() {
       return this.prefix;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }
}
