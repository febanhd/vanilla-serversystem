package de.febanhd.serversystem.manager;

import de.febanhd.serversystem.ServerSystem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.time.LocalDate;
import java.time.LocalTime;

public class TablistManager {

    public void registerPlayerInTablist(Player player) {
        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
        Team admin = board.registerNewTeam(Teams.ADMIN.getTeamName());
        Team gelb = board.registerNewTeam(Teams.GELB.getTeamName());
        Team rot = board.registerNewTeam(Teams.ROT.getTeamName());
        Team grün = board.registerNewTeam(Teams.GRÜN.getTeamName());
        Team blau = board.registerNewTeam(Teams.BLAU.getTeamName());
        Team noplayer = board.registerNewTeam(Teams.NOPLAYER.getTeamName());
        admin.setPrefix(Teams.ADMIN.getPrefix());
        admin.setColor(Teams.ADMIN.getColor());
        gelb.setPrefix(Teams.GELB.getPrefix());
        gelb.setColor(Teams.GELB.getColor());
        rot.setPrefix(Teams.ROT.getPrefix());
        rot.setColor(Teams.ROT.getColor());
        grün.setPrefix(Teams.GRÜN.getPrefix());
        grün.setColor(Teams.GRÜN.getColor());
        blau.setPrefix(Teams.BLAU.getPrefix());
        blau.setColor(Teams.BLAU.getColor());
        noplayer.setPrefix(Teams.NOPLAYER.getPrefix());
        noplayer.setColor(Teams.NOPLAYER.getColor());
        for(Player players : Bukkit.getOnlinePlayers()) {
            Teams playerTeam = ServerSystem.getInstance().getDataManager().getTeam(players);
            players.setDisplayName(playerTeam.getPrefix() + players.getName());
            switch(playerTeam) {
                case ADMIN:
                    admin.addEntry(players.getName());
                    break;
                case ROT:
                    rot.addEntry(players.getName());
                    break;
                case GRÜN:
                    grün.addEntry(players.getName());
                    break;
                case BLAU:
                    blau.addEntry(players.getName());
                    break;
                case GELB:
                    gelb.addEntry(players.getName());
                    break;
                default:
                    noplayer.addEntry(players.getName());
                    break;
            }
            for(Player all : Bukkit.getOnlinePlayers()) {
                all.setScoreboard(board);
            }
        }
    }

    public void setHeaderAndFooter(boolean onJoin) {
        int size = Bukkit.getOnlinePlayers().size();
        if(onJoin) {
            size++;
        }
        for(Player player : Bukkit.getOnlinePlayers()) {
            player.setPlayerListHeader("§2Minecraft §cProjekt §7by FebanHD\n" +
                                        " \n" +
                                        "§fOnline: §c" + size +
                                        "\n ");
            player.setPlayerListFooter(" \n" +
                    "§fUhrzeit: §c" + this.getTime() + "\n" +
                    "§fDatum: §c" + this.getDate() +
                    "\n ");
        }
    }

    private String getTime() {
        LocalTime localTime = LocalTime.now();
        String h = localTime.getHour() + "";
        String m = localTime.getMinute() + "";
        String s = localTime.getSecond() + "";
        if(localTime.getHour() < 10) {
            h = "0" + h;
        }
        if(localTime.getMinute() < 10) {
            m = "0" + m;
        }
        if(localTime.getSecond() < 10) {
            s = "0" + s;
        }
        return h + ":" + m + ":" + s;
    }

    private String getDate() {
        LocalDate localDate = LocalDate.now();
        String d =  localDate.getDayOfMonth() + "";
        String m = localDate.getMonthValue() + "";
        String y = localDate.getYear() + "";
        if(localDate.getDayOfMonth() < 10) {
            d = "0" + d;
        }
        if(localDate.getMonthValue() < 10) {
            m = "0" + m;
        }
        return d + "." + m + "." + y;
    }

}
