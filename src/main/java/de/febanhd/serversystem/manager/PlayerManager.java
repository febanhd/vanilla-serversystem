package de.febanhd.serversystem.manager;

import com.google.common.collect.Lists;
import de.febanhd.serversystem.ServerSystem;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class PlayerManager {

    private ArrayList<SystemPlayer> players = Lists.newArrayList();

    public void registerPlayer(Player p) {
        SystemPlayer player = new SystemPlayer(false, p, Teams.NOPLAYER);
        this.players.add(player);
        DataManager dataManager = ServerSystem.getInstance().getDataManager();
        dataManager.checkPlayer(p);
        player.setTeam(dataManager.getTeam(player.toBukkitPlayer()));
        if(dataManager.canPlay(p.getUniqueId())) {
            player.setCanPlay(true);
        }
    }

    public SystemPlayer getPlayer(final String name) {
        for(SystemPlayer player : this.players) {
            if(player.toBukkitPlayer().getName().equalsIgnoreCase(name)) {
                return player;
            }
        }
        return null;
    }

    public SystemPlayer getPlayer(final UUID uuid) {
        for(SystemPlayer player : this.players) {
            if(player.toBukkitPlayer().getUniqueId().equals(uuid)) {
                return player;
            }
        }
        return null;
    }

    public ArrayList<SystemPlayer> getPlayers() {
        return players;
    }
}
