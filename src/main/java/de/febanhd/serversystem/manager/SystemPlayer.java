package de.febanhd.serversystem.manager;

import org.bukkit.entity.Player;

import java.util.UUID;

public class SystemPlayer {

    private boolean canPlay;
    private Player bukkitPlayer;
    private Teams team;

    public SystemPlayer(boolean canPlay, Player bukkitPlayer, Teams team) {
        this.canPlay = canPlay;
        this.bukkitPlayer = bukkitPlayer;
        this.team = team;
    }

    public void sendMessage(String msg) {
        this.bukkitPlayer.sendMessage(msg);
    }

    public Player toBukkitPlayer() {
        return this.bukkitPlayer;
    }

    public void setCanPlay(boolean canPlay) {
        this.canPlay = canPlay;
    }

    public boolean canPlay() {
        return this.canPlay;
    }

    public Teams getTeam() {
        return team;
    }

    public void setTeam(Teams team) {
        this.team = team;
    }

    public UUID getUUID() {
        return this.bukkitPlayer.getUniqueId();
    }
}
