package de.febanhd.serversystem.sql;

import java.sql.*;
import java.util.concurrent.CompletableFuture;


public class MySQL {
    private String HOST = "";
    private String DATABASE = "";
    private String USER = "";
    private String PASSWORD = "";

    private Connection con;

    public MySQL(String HOST, String DATABASE, String USER, String PASSWORD) {
        this.HOST = HOST;
        this.DATABASE = DATABASE;
        this.USER = USER;
        this.PASSWORD = PASSWORD;

    }

    public MySQL(String HOST, String DATABASE, String USER) {
        this.HOST = HOST;
        this.DATABASE = DATABASE;
        this.USER = USER;
    }

    public void connect() {
        try {
            if (con != null) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");


            con = DriverManager.getConnection("jdbc:mysql://" + HOST + ":3306/" + DATABASE + "?autoReconnect=true", USER, PASSWORD);
            System.out.println("MYSQL Verbindung steht!");

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void close() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return con != null;
    }

    public void update(String qry) {
        if (isConnected()) {
            try {
                java.sql.PreparedStatement ps = con.prepareStatement(qry);
                ps.executeUpdate(qry);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ResultSet query(String query) throws SQLException {
        PreparedStatement statement = con.prepareStatement(query);
        return statement.executeQuery();
    }

    public Connection getConnection() {
        return con;
    }
}

