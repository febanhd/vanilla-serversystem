package de.febanhd.serversystem;

import de.febanhd.serversystem.commands.AcceptCommand;
import de.febanhd.serversystem.commands.ListRequestsCommand;
import de.febanhd.serversystem.commands.MitspielenCommand;
import de.febanhd.serversystem.commands.RemoveCommand;
import de.febanhd.serversystem.listener.CancelListener;
import de.febanhd.serversystem.listener.ChatListener;
import de.febanhd.serversystem.listener.PlayerConnectionListener;
import de.febanhd.serversystem.listener.ProtectionListener;
import de.febanhd.serversystem.manager.DataManager;
import de.febanhd.serversystem.manager.PlayerManager;
import de.febanhd.serversystem.manager.TablistManager;
import de.febanhd.serversystem.protection.ProtectionManager;
import de.febanhd.serversystem.sql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ServerSystem extends JavaPlugin {

    static ServerSystem instance;

    public static MySQL mySQL;
    public static final String PREFIX = "§7[§bSystem§7] §r";
    private PlayerManager playerManager;
    private DataManager dataManager;
    private TablistManager tablistManager;
    private ProtectionManager protectionManager;

    @Override
    public void onEnable() {
        ServerSystem.instance = this;

        mySQL = new MySQL("localhost", "system", "root", "");
        mySQL.connect();
        if(mySQL.isConnected()) {

            mySQL.update("CREATE TABLE IF NOT EXISTS serversystem (UUID VARCHAR(100) PRIMARY KEY," +
                    "team VARCHAR(40) NOT NULL," +
                    "canPlay BOOLEAN NOT NULL," +
                    "name VARCHAR(50) NOT NULL," +
                    "request BOOLEAN NOT NULL," +
                    "requestTeam VARCHAR(40) NOT NULL)");

            this.playerManager = new PlayerManager();
            this.dataManager = new DataManager(mySQL);
            this.tablistManager = new TablistManager();
            this.protectionManager = new ProtectionManager();
            PluginManager pm = Bukkit.getPluginManager();
            pm.registerEvents(new PlayerConnectionListener(), this);
            pm.registerEvents(new ChatListener(), this);
            pm.registerEvents(new CancelListener(), this);
            pm.registerEvents(new ProtectionListener(), this);

            getCommand("mitspielen").setExecutor(new MitspielenCommand());
            getCommand("accept").setExecutor(new AcceptCommand());
            getCommand("listrequests").setExecutor(new ListRequestsCommand());
            getCommand("remove").setExecutor(new RemoveCommand());

            for(Player all : Bukkit.getOnlinePlayers()) {
                this.playerManager.registerPlayer(all);
                this.tablistManager.registerPlayerInTablist(all);
            }

            this.secondScheduler();
        }
    }

    public void secondScheduler() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                ServerSystem.getInstance().getTablistManager().setHeaderAndFooter(false);
            }
        }, 20, 20);
    }

    @Override
    public void onDisable() {
        mySQL.close();
    }

    public static ServerSystem getInstance() {
        return instance;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public TablistManager getTablistManager() {
        return tablistManager;
    }

    public ProtectionManager getProtectionManager() {
        return protectionManager;
    }
}
