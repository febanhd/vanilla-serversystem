package de.febanhd.serversystem.listener;

import de.febanhd.serversystem.ServerSystem;
import de.febanhd.serversystem.manager.Teams;
import de.febanhd.serversystem.protection.*;
import de.febanhd.serversystem.protection.impl.PrivateProtection;
import de.febanhd.serversystem.protection.Protection;
import de.febanhd.serversystem.protection.impl.TeamProtection;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Sign;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.UUID;

public class ProtectionListener implements Listener {


    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        Block block = event.getBlock();
        if (block.getType() == Material.SPRUCE_WALL_SIGN ||
                block.getType() == Material.ACACIA_WALL_SIGN ||
                block.getType() == Material.BIRCH_WALL_SIGN ||
                block.getType() == Material.DARK_OAK_WALL_SIGN ||
                block.getType() == Material.JUNGLE_WALL_SIGN ||
                block.getType() == Material.OAK_WALL_SIGN) {
            Sign sign = (Sign) block.getState();
            BlockFace blockFace = ((Directional) block.getBlockData()).getFacing();
            Block blockBehindSign = null;
            switch (blockFace) {
                case WEST:
                    blockBehindSign = block.getRelative(BlockFace.EAST);
                    break;
                case EAST:
                    blockBehindSign = block.getRelative(BlockFace.WEST);
                    break;
                case NORTH:
                    blockBehindSign = block.getRelative(BlockFace.SOUTH);
                    break;
                case SOUTH:
                    blockBehindSign = block.getRelative(BlockFace.NORTH);
                    break;
                default:
                    break;
            }
            if(event.getLine(0).equals("[Team]")) {
                sign.setEditable(false);
                Teams team =ServerSystem.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).getTeam();
                ServerSystem.getInstance().getProtectionManager().protectBlockTeam(blockBehindSign, team);
                event.getPlayer().sendMessage(ServerSystem.PREFIX + "§aProtection erfolgreich erstellt.");
            }
            if(event.getLine(0).equals("[Privat]")) {
                sign.setEditable(false);
                ServerSystem.getInstance().getProtectionManager().protectBlockPrivate(blockBehindSign, event.getPlayer().getUniqueId());
                event.getPlayer().sendMessage(ServerSystem.PREFIX + "§aProtection erfolgreich erstellt.");
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (block.getType() == Material.SPRUCE_WALL_SIGN ||
                block.getType() == Material.ACACIA_WALL_SIGN ||
                block.getType() == Material.BIRCH_WALL_SIGN ||
                block.getType() == Material.DARK_OAK_WALL_SIGN ||
                block.getType() == Material.JUNGLE_WALL_SIGN ||
                block.getType() == Material.OAK_WALL_SIGN) {
            BlockFace blockFace = ((Directional) block.getBlockData()).getFacing();
            Block blockBehindSign = null;
            switch (blockFace) {
                case WEST:
                    blockBehindSign = block.getRelative(BlockFace.EAST);
                    break;
                case EAST:
                    blockBehindSign = block.getRelative(BlockFace.WEST);
                    break;
                case NORTH:
                    blockBehindSign = block.getRelative(BlockFace.SOUTH);
                    break;
                case SOUTH:
                    blockBehindSign = block.getRelative(BlockFace.NORTH);
                    break;
                default:
                    break;
            }
            ProtectionManager protectionManager = ServerSystem.getInstance().getProtectionManager();
            if(protectionManager.isProtected(blockBehindSign.getLocation())) {
                Protection protection = protectionManager.getProtecion(blockBehindSign.getLocation());
                if(canOpen(event.getPlayer(), blockBehindSign)) {
                    ServerSystem.getInstance().getProtectionManager().removeProtection(protection);
                    event.getPlayer().sendMessage(ServerSystem.PREFIX + "§aProtection erfolgreich §centfernt§a.");
                }else {
                    event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
            if(!this.canOpen(event.getPlayer(), event.getClickedBlock())) {
                event.setCancelled(true);
            }
        }
    }

    public boolean canOpen(Player player, Block block) {
        if(ServerSystem.getInstance().getPlayerManager().getPlayer(player.getUniqueId()).getTeam() == Teams.ADMIN) return true;
        ProtectionManager protectionManager = ServerSystem.getInstance().getProtectionManager();
        if(protectionManager.isProtected(block.getLocation())) {
            Protection protection = protectionManager.getProtecion(block.getLocation());
            ProtectionTyp protectionTyp = protection.getTyp();
            if(protectionTyp == ProtectionTyp.PRIVATE) {
                PrivateProtection privateProtection = (PrivateProtection) protection;
                if(privateProtection.getPlayerUUID().toString() != player.getUniqueId().toString()) {
                    player.sendMessage(ServerSystem.PREFIX + "§7Dieser Block ist Privat geschützt.");
                    return false;
                }else {
                    return true;
                }
            }
            if(protectionTyp == ProtectionTyp.TEAM) {
                TeamProtection teamProtectionProtection = (TeamProtection) protection;
                Teams playerTeam = ServerSystem.getInstance().getPlayerManager().getPlayer(player.getUniqueId()).getTeam();
                if(teamProtectionProtection.getTeam() != playerTeam) {
                    player.sendMessage(ServerSystem.PREFIX + "§7Dieser Block ist geschützt und kann nur von Team " + teamProtectionProtection.getTeam().getName() + " §7geöffnet werden.");
                    return false;
                }else {
                    return true;
                }
            }
        }
        return true;
    }
}
