package de.febanhd.serversystem.listener;

import de.febanhd.serversystem.ServerSystem;
import de.febanhd.serversystem.manager.SystemPlayer;
import de.febanhd.serversystem.manager.Teams;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        event.setCancelled(true);
        if(event.getMessage().startsWith("@t") || event.getMessage().startsWith("@T")) {
            Teams team = ServerSystem.getInstance().getPlayerManager().getPlayer(player.getUniqueId()).getTeam();
            for(SystemPlayer all : ServerSystem.getInstance().getPlayerManager().getPlayers()) {
                if(all.getTeam().equals(team)) {
                    all.sendMessage(team.getColor() + "Team §8- §r" + player.getDisplayName() + "§8:§7" + event.getMessage().replace(event.getMessage().split(" ")[0], ""));
                }
            }
            return;
        }
        for(Player all : Bukkit.getOnlinePlayers()) {
            int distance = (int) Math.round(player.getLocation().distance(all.getLocation()));
            if(player.getLocation().distance(all.getLocation()) <= 100) {
                all.sendMessage("§8[§7" + distance + "B§8] " + player.getDisplayName() + "§8: §f" + event.getMessage());
            }else if(player.getLocation().distance(all.getLocation()) <= 500) {
                all.sendMessage("§8[§7" + distance + "B§8] " + player.getDisplayName() + "§8: §7" + event.getMessage());
            }else {
                all.sendMessage("§8[§7" + distance + "B§8] " + player.getDisplayName() + "§8: §8" + event.getMessage());
            }
        }
    }

}
