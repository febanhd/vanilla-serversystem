package de.febanhd.serversystem.listener;

import de.febanhd.serversystem.ServerSystem;
import de.febanhd.serversystem.manager.Teams;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerConnectionListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        Player player = event.getPlayer();
        ServerSystem.getInstance().getPlayerManager().registerPlayer(player);
        ServerSystem.getInstance().getTablistManager().registerPlayerInTablist(player);
        Bukkit.broadcastMessage(ServerSystem.PREFIX + player.getDisplayName() + " §7hat den Server §abetreten§7.");
        if(!ServerSystem.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).canPlay()) {
            player.sendMessage("§7Willkommen, bitte benutze §a/mitspielen §7um eine Mitspiel-Anfrage zu stellen.");
        }
        if(ServerSystem.getInstance().getDataManager().getTeam(player).equals(Teams.ADMIN)) {
            player.chat("/listrequests");
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        ServerSystem.getInstance().getPlayerManager().getPlayers().remove(ServerSystem.getInstance().getPlayerManager().getPlayer(player.getUniqueId()));
        event.setQuitMessage(ServerSystem.PREFIX + player.getDisplayName() + " §7hat den Server §cverlassen§7.");
        ServerSystem.getInstance().getTablistManager().setHeaderAndFooter(false);
    }
}
