package de.febanhd.serversystem.listener;

import de.febanhd.serversystem.ServerSystem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class CancelListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        if(!ServerSystem.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).canPlay()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if(!ServerSystem.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).canPlay()) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if(!ServerSystem.getInstance().getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).canPlay()) {
            event.setCancelled(true);
        }
    }

}
