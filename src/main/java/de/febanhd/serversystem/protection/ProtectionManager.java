package de.febanhd.serversystem.protection;

import com.google.common.collect.Lists;
import de.febanhd.serversystem.manager.Teams;
import de.febanhd.serversystem.protection.impl.PrivateProtection;
import de.febanhd.serversystem.protection.impl.TeamProtection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class ProtectionManager {

    private ArrayList<Protection> protections = Lists.newArrayList();

    private File file = new File("plugins/ServerSystem", "protections.yml");
    private FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

    public ProtectionManager() {
        this.loadProtections();
    }

    public void protectBlockPrivate(Block block, UUID playerUUID) {
        UUID uuid = UUID.randomUUID();
        this.protections.add(new PrivateProtection(block.getLocation(), ProtectionTyp.PRIVATE, playerUUID, uuid));
        Location loc = block.getLocation();
        cfg.set("Protections.Private." + playerUUID.toString() + "." + uuid.toString() + ".Location.World", loc.getWorld().getName());
        cfg.set("Protections.Private." + playerUUID.toString() + "." + uuid.toString() + ".Location.X", loc.getBlockX());
        cfg.set("Protections.Private." + playerUUID.toString() + "." + uuid.toString() + ".Location.Y", loc.getBlockY());
        cfg.set("Protections.Private." + playerUUID.toString() + "." + uuid.toString() + ".Location.Z", loc.getBlockZ());
        this.saveConfig();
    }

    public void protectBlockTeam(Block block, Teams team) {
        UUID uuid = UUID.randomUUID();
        this.protections.add(new TeamProtection(block.getLocation(), ProtectionTyp.TEAM, team, uuid));
        Location loc = block.getLocation();
        cfg.set("Protections.Teams." + team.toString() + "." + uuid.toString() + ".Location.World", loc.getWorld().getName());
        cfg.set("Protections.Teams." + team.toString() + "." + uuid.toString() + ".Location.X", loc.getBlockX());
        cfg.set("Protections.Teams." + team.toString() + "." + uuid.toString() + ".Location.Y", loc.getBlockY());
        cfg.set("Protections.Teams." + team.toString() + "." + uuid.toString() + ".Location.Z", loc.getBlockZ());
        this.saveConfig();
    }

    private void loadProtections() {
        if(cfg.contains("Protections.Teams")) {
            Set<String> teams = cfg.getConfigurationSection("Protections.Teams").getKeys(false);
            for (String teamName : teams) {
                Set<String> uuids = cfg.getConfigurationSection("Protections.Teams." + teamName).getKeys(false);
                for (String uuid : uuids) {
                    this.protections.add(new TeamProtection(this.getLocation("Protections.Teams." + teamName + "." + uuid + ".Location"), ProtectionTyp.TEAM, Teams.valueOf(teamName), UUID.fromString(uuid)));
                }
            }
        }
        if(cfg.contains("Protections.Private")) {
            Set<String> players = cfg.getConfigurationSection("Protections.Private").getKeys(false);
            for (String playerUUID : players) {
                Set<String> uuids = cfg.getConfigurationSection("Protections.Private." + playerUUID).getKeys(false);
                for (String uuid : uuids) {
                    this.protections.add(new PrivateProtection(this.getLocation("Protections.Private." + playerUUID + "." + uuid + ".Location"), ProtectionTyp.TEAM, UUID.fromString(playerUUID), UUID.fromString(uuid)));
                }
            }
        }
        System.out.println("Geladene Protections: " + this.protections.size());
    }

    public void removeProtection(Protection protection) {
        if(protection.getTyp().equals(ProtectionTyp.TEAM)) {
            Teams team = ((TeamProtection)protection).getTeam();
            cfg.set("Protections.Teams." + team.toString() + "." + protection.getUuid().toString(), null);
        }
        if(protection.getTyp().equals(ProtectionTyp.PRIVATE)) {
            UUID playerUUID = ((PrivateProtection)protection).getPlayerUUID();
            cfg.set("Protections.Teams." + playerUUID.toString() + "." + protection.getUuid().toString(), null);
        }
        this.saveConfig();
    }

    private Location getLocation(String path) {
        World world = Bukkit.getWorld(cfg.getString(path + ".World"));
        double x = cfg.getDouble(path + ".X");
        double y = cfg.getDouble(path + ".Y");
        double z = cfg.getDouble(path + ".Z");
        return new Location(world, x, y, z);

    }

    public ArrayList<Protection> getProtections() {
        return protections;
    }

    public boolean isProtected(Location location) {
        for(Protection protection : this.protections) {
            if(protection.getBlock().equals(location.getBlock())) {
                return true;
            }
        }
        return false;
    }

    public Protection getProtecion(Location location) {
        for(Protection protection : this.protections) {
            if(protection.getBlock().equals(location.getBlock())) {
                return protection;
            }
        }
        return null;
    }

    private void saveConfig() {
        try {
            cfg.save(file);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
