package de.febanhd.serversystem.protection;

import de.febanhd.serversystem.manager.Teams;
import de.febanhd.serversystem.protection.ProtectionTyp;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.util.UUID;

public abstract class Protection {

    private Location blockLocation;
    private ProtectionTyp typ;
    private UUID uuid;


    public Protection(Location location, ProtectionTyp typ, UUID uuid) {
        this.blockLocation = location;
        this.typ = typ;
        this.uuid = uuid;
    }

    public Location getBlockLocation() {
        return blockLocation;
    }

    public Block getBlock() {
        Block block = this.blockLocation.getBlock();
        return block;
    }

    public ProtectionTyp getTyp() {
        return typ;
    }

    public UUID getUuid() {
        return uuid;
    }
}
