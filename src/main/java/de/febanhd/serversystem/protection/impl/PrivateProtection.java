package de.febanhd.serversystem.protection.impl;

import de.febanhd.serversystem.protection.ProtectionTyp;
import de.febanhd.serversystem.protection.Protection;
import org.bukkit.Location;

import java.util.UUID;

public class PrivateProtection extends Protection {

    private UUID playerUUID;

    public PrivateProtection(Location location, ProtectionTyp typ, UUID playerUUID, UUID uuid) {
        super(location, typ, uuid);
        this.playerUUID = playerUUID;
    }

    public UUID getPlayerUUID() {
        return playerUUID;
    }
}
