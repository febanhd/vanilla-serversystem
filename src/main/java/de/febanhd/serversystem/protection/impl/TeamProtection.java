package de.febanhd.serversystem.protection.impl;

import de.febanhd.serversystem.manager.Teams;
import de.febanhd.serversystem.protection.ProtectionTyp;
import de.febanhd.serversystem.protection.Protection;
import org.bukkit.Location;

import java.util.UUID;

public class TeamProtection extends Protection {

    private Teams team;

    public TeamProtection(Location location, ProtectionTyp typ, Teams team, UUID uuid) {
        super(location, typ, uuid);
        this.team = team;
    }

    public Teams getTeam() {
        return team;
    }
}
