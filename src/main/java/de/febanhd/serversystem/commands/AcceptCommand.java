package de.febanhd.serversystem.commands;

import de.febanhd.serversystem.ServerSystem;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class AcceptCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender.isOp()) {
            if(args.length == 1) {
                String playerName = args[0];
                if(ServerSystem.getInstance().getDataManager().hasRequestSended(playerName)) {
                    ServerSystem.getInstance().getDataManager().acceptPlayer(ServerSystem.getInstance().getDataManager().getUUID(playerName));
                    sender.sendMessage(ServerSystem.PREFIX + "§a" + playerName + " ist nun ein Mitspieler.");
                }else {
                    sender.sendMessage(ServerSystem.PREFIX + "§cDieser Spieler hat keine Anfrage gesendet.");
                }
            }
        }
        return false;
    }
}
