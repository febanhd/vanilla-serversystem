package de.febanhd.serversystem.commands;

import de.febanhd.serversystem.ServerSystem;
import de.febanhd.serversystem.manager.DataManager;
import de.febanhd.serversystem.manager.SystemPlayer;
import de.febanhd.serversystem.manager.Teams;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MitspielenCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            SystemPlayer player = ServerSystem.getInstance().getPlayerManager().getPlayer(((Player)sender).getUniqueId());
            DataManager dataManager = ServerSystem.getInstance().getDataManager();
            if(dataManager.getTeam(player.toBukkitPlayer()).equals(Teams.NOPLAYER)) {
                if(args.length == 1) {
                    if(isTeam(args[0].toUpperCase())) {
                        Teams team = Teams.valueOf(args[0].toUpperCase());
                        if(dataManager.requestForPlayer(player.toBukkitPlayer(), team)) {
                            player.sendMessage(ServerSystem.PREFIX + "§7Du hast die §aAnfrage §7abgeschickt. Bitte warte bis Sie ein §4Administrator §7akzeptiert.");
                        }else {
                            player.sendMessage(ServerSystem.PREFIX + "§cDu hast bereits eine Anfrage gestellt. Gedulde dich bis Sie bearbeitet wird.");
                        }
                    }else {
                        player.sendMessage(ServerSystem.PREFIX + "§7Benutze: §e/" + label + " [ROT/GELB/GRÜN/BLAU]");
                    }
                }else {
                    player.sendMessage(ServerSystem.PREFIX + "§7Benutze: §e/" + label + " [ROT/GELB/GRÜN/BLAU]");
                }
            }else {
                player.sendMessage(ServerSystem.PREFIX + "§cDu bist bereits ein Mitspieler.");
            }
        }
        return false;
    }

    public boolean isTeam(String input) {
        try {
            Teams.valueOf(input);
            return true;
        }catch (Exception e) {
            return false;
        }
    }
}
