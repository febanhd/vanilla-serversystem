package de.febanhd.serversystem.commands;

import de.febanhd.serversystem.ServerSystem;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ListRequestsCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if(sender instanceof Player) {
            Player player = (Player) sender;
            if(player.isOp()) {
                player.sendMessage("§aAlle Anfragen:");
                for (String names : ServerSystem.getInstance().getDataManager().getRequestedPlayerNames()) {
                    TextComponent text = new TextComponent("§8- §7" + names);
                    text.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/accept " + names));
                    player.spigot().sendMessage(text);
                }
            }
        }
        return false;
    }
}
