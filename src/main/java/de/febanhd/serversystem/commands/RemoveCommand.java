package de.febanhd.serversystem.commands;

import de.febanhd.serversystem.ServerSystem;
import de.febanhd.serversystem.manager.DataManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.UUID;

public class RemoveCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender.isOp()) {
            if(args.length == 1) {
                String playerName = args[0];
                DataManager dataManager = ServerSystem.getInstance().getDataManager();
                UUID uuid = dataManager.getUUID(playerName);
                if(uuid != null) {
                    dataManager.removePlayer(uuid);
                    sender.sendMessage(ServerSystem.PREFIX + "§a" + playerName + "§7wurde erfolgreich aus der Mitspieler-Liste entfernt.");
                }else {
                    sender.sendMessage(ServerSystem.PREFIX + "§cDieser Spieler war noch nie auf dem Server.");
                }
            }else {
                sender.sendMessage(ServerSystem.PREFIX + "§7Benutze: §e/" + label + " <Spielername>");
            }
        }
        return false;
    }

}
